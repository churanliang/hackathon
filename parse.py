from bs4 import BeautifulSoup
import requests

soup = BeautifulSoup(open('acro.html'), 'html.parser')
table = soup.find('table', attrs={'class': 'tablesorter'})
body = table.find('tbody')
rows = body.find_all('tr')

acro = open('acro.txt', 'w')

for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    fin = [ele for ele in cols if ele]
    if len(fin) > 1:
        try:
            acro.write(str(fin[0]) + '\t' + str(fin[1]) + '\n')
        except:
            print 'hi'

acro.close();
