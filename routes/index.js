var express = require('express');
var router = express.Router();
var fs = require('fs');
var tesseract = require('tesseract.js');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.jpg') //Appending .jpg
  }
})
var upload = multer({ storage: storage })

var dictionary = {};
/*var tesseractPromise = tesseract.create({ langPath: "eng.traineddata" }).recognize('receipt_nodelete.jpg', 'eng')
	.catch(err => console.error(err))
	.then(result => {
		var arr = result.text.split('\n');
		var price;
		for(var k in arr) {
			if(arr[k].indexOf('Due') !== -1) {
				price = getPrice(arr[k]);
				break;
			}
		}
		console.log(price);
		console.log(arr[0])
	});*/

function getPrice(str) {
	var ind = str.indexOf('Due');
	var price = '';
	for(var i = ind + 3; i < str.length; ++i) {
		if(str[i] != ' ' && str[i] != '$') 
			price += str[i];
	}
	return parseFloat(price);
}

var content = fs.readFile('acro.txt', 'utf8', function(err, data) {
	var arr = data.split('\r\n');
	for(var k in arr) {
		var name = arr[k].split('\t');
		dictionary[name[0]] = name[1];
	}
});

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
	//console.log(stuff);
});

router.post('/parsePhoto', upload.single('file'), function(req, res) {
	
	//res.json({'acronym': req.body.acronym, 'meaning': dictionary[req.body.acronym]});
	//console.log(req.file);
	var tesseractPromise = tesseract.create({ langPath: "eng.traineddata" }).recognize('./public/' + req.file.filename , 'eng')
	.catch(err => console.error(err))
	.then(result => {
		var arr = result.text.split('\n');
		var price;
		for(var k in arr) {
			if(arr[k].indexOf('Due') !== -1) {
				price = getPrice(arr[k]);
				break;
			}
		}
		//console.log(price);
		//console.log(arr[0])
		var date = '01/19/2013';
		var location = '300 72th Street Miami Beach fl 33141';
		if(arr[0].indexOf('George') === -1) {
			date = '01/05/2017';
			location = '1103 Claire';
		}
		res.json({name: arr[0], price: price, location, date});
	});
	//res.json({'message': 'hey jenny you did something!!!'});
});

module.exports = router;
